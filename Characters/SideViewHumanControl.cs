using UnityEngine;
using System.Collections;

public class SideViewHumanControl : SideViewCharacterControl {

	Animator animator;
	SideViewCharacterMotor motor;

	void Awake () {
		animator = this.GetComponentOrFail<Animator>();
		motor = this.GetComponentOrFail<SideViewCharacterMotor>();
	}

	// Update is called once per frame
	void Update () {
		Vector2 moveInputVector = new Vector2(
			Input.GetAxis("Move Horizontal"),
			Input.GetAxis("Move Vertical")
			);
		m_MoveIntentionVector = moveInputVector * motor.maxSpeed;

		// record jump input if grounded (if false, do nothing so as not to cancel an input if there are multiple Update between two FixedUpdates)
		if (animator.GetBool("Grounded") && Input.GetButtonDown("Jump")) {
			m_JumpIntention = true;
			Debug.Log("Jump input");
		}
	}

}
