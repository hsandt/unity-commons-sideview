using UnityEngine;
using System.Collections;

public class SideViewCharacter_MotionState : StateMachineBehaviour {

	// scripts
	protected SideViewCharacterControl control;
	protected SideViewCharacterMotor motor;
	protected Rigidbody2D rigidbody2d;

	virtual public void Init(SideViewCharacterControl control, SideViewCharacterMotor motor, Rigidbody2D rigidbody2d) {
		this.control = control;
		this.motor = motor;
		this.rigidbody2d = rigidbody2d;
	}


}
